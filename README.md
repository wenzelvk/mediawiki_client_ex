# Elixir MediaWiki client

Unofficial client modules for connecting to Wikipedia, Wikidata, and other MediaWiki sites from Elixir code.  Supports multiple APIs: Action, EventStreams, and Scoring.

API reference for the current version: [Documentation and examples](https://hexdocs.pm/mediawiki_client/api-reference.html)

Overview:
* `Wiki.Action` is the primary read/write interface for wikis, connecting to the
[Action API](https://www.mediawiki.org/wiki/Special:MyLanguage/API:Main_page).
This is a rich set of commands to query or edit almost anything on a wiki.
* `Wiki.EventStreams` is a real-time
[EventStreams](https://wikitech.wikimedia.org/wiki/Event_Platform/EventStreams)
feed allowing an application to process edits as they happen.
* `Wiki.Ores` is a [scoring service](https://www.mediawiki.org/wiki/ORES)
for estimating revision and edit quality.

## Installation

Install this package by adding `mediawiki_client` to your dependencies in `mix.exs`,

```elixir
def deps do
  [
    {:mediawiki_client, "~> 0.4.0"}
  ]
end
```

Documentation is generated with `mix docs`.

## Updating

The [change log](CHANGELOG.md) will have information about new features and any
breaking changes.  Expect the interfaces to evolve until the 1.0 release is reached.

## Livebook

An included [Livebook](https://livebook.dev/) notebook demonstrates the
library's capabilities.  First, install or update livebook locally:

```
mix do local.rebar --force, local.hex --force
mix escript.install hex livebook
```

Make local escripts available by editing your `~/.profile`, if you find that
"livebook" still can't be found.  You may have to restart the shell for changes
to take effect.

```
export PATH="$PATH:$HOME/.mix/escripts"
```

Now, run the notebook server:

```
livebook server notebooks/demo.livemd
```

## Usage

Fetch some statistics about German Wikipedia,

```elixir
Wiki.SiteMatrix.new()
|> Wiki.SiteMatrix.get!("dewiki")
|> Wiki.Action.new()
|> Wiki.Action.get!(
  action: :query,
  meta: :siteinfo,
  siprop: :statistics
)
# %Wiki.Action.Session{
#   ...
#   result: %{
#     "batchcomplete" => true,
#     "query" => %{
#       "statistics" => %{
#         "activeusers" => 19687,
#         "admins" => 188,
#         "articles" => 2583285,
#         "edits" => 211219883,
#         "images" => 130199,
#         "jobs" => 0,
#         "pages" => 7163473,
#         "queued-massmessages" => 0,
#         "users" => 3715461
#       }
#     }
#   },
#   ...
# }
```

See the [source documentation](https://hexdocs.pm/mediawiki_client/api-reference.html)
for more examples.

### Error handling

Methods come in an assertive and a non-assertive form, for example the
`Wiki.Action.get()` method returns an `{:ok, ...}` or a `{:error, ...}` tuple, and
the `Wiki.Action.get!()` method raises an error directly.

## Development

The [project homepage](https://gitlab.com/adamwight/mediawiki_client_ex) is on GitLab.
To contribute, feel free to start with an issue, push a merge request, or
contact maintainers.

To run all tests,
```shell
mix test.all
```

To generate a test [coverage report](../cover/excoveralls.html),
```shell
mix coveralls.html
```
