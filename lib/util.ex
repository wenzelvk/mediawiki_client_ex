defmodule Wiki.Util do
  @moduledoc false

  @version Mix.Project.config()[:version]

  @doc false
  @spec default_user_agent() :: String.t()
  def default_user_agent do
    "mediawiki_client_ex/#{@version} (spam@ludd.net)"
  end
end

defmodule Wiki.Error do
  @moduledoc false

  # TODO: more nuanced error types that can be matched by consumers.
  @doc false
  defexception [:message]
end
