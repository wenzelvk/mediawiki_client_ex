# Change log

## [Unreleased]

### Changed

- Default Action API timeout changed from 5s to 60s.
- Changelog reformatted according to the [Common Changelog](https://common-changelog.org/) style.

## [0.4.3] - 2023-04

### Changed

* Action API timeout extended from 5 to 60 seconds.

## [0.4.2] - 2023-03

### Changed

- **Breaking:** Require Elixir 1.11

### Added

- Add a Livebook example notebook

## [0.4.1] - 2022-12

### Added

- Now compatible with tesla ~>1.5

### Fixed

- Fix compression for Action and ORES APIs

## [0.4.0] - 2022-09

### Changed

- **Breaking:** Wiki.EventStreams `:send_to` option renamed to `:stream_to`.
- `mix test.all` task replaces git\_hooks for development.

### Removed

- **Breaking:** Config variables are deprecated, all customization is done via keyword options.

## [0.3.1] - 2022-09

### Changed

- **Breaking:** Wiki.Site renamed to Wiki.SiteMatrix and has an updated interface.  `new()` returns an opaque sitematrix.
- **Breaking:** Settle on the Apache 2 license (bug #21).

### Added

- Wiki.SiteMatrix caches the response.

## [0.3.0] - 2021-12

### Changed

- **Breaking:** Switched most of the API to return `{:ok | :error, ...}` tuples.  For quick migration just use the bang functions like `get!`.  Or match `{:ok, result}` for fine-grained error handling.  Errors will be returned as exception objects, with a crude string message for now.  In a future release these will include a reason atom.

## [0.2.6] - 2021-12

### Changed

- **Breaking:** Renamed the package from `wiki_elixir` to `mediawiki_client`.  `mix.exs` dependencies must be updated.  The package namespace is still `Wiki.`-, because I honor your labors, gentle reader.
- **Breaking:** OTP app name in configuration must also be changed from `wiki_elixir` to `mediawiki_client`.

## [0.2.5] - 2021-11

### Changed

- You may migrate `Wiki.Action.new()` calls to accept a `Wiki.Site` object, which will be used to calculate the default endpoint.

### Added

- `Wiki.Site` module for retrieving the Wikimedia site matrix.

## [0.2.2] - 2021-11

### Changed

- Depend on eventsource\_ex library, testability improvements have been upstreamed.

### Added

- Continuous tests verify compatibility with Elixir 1.9-1.12 .

### Removed

- Drop unused dependencies.

### Fixed

- User-agent header sends correct library version.

## [0.2.1] - 2021-05

### Changed

- **Breaking:** Request query parameters are passed as a keyword list rather than a map.
- **Breaking:** Handles application-level API errors in the action and ores apis, failing fast.
- Default to action response format version 2.

### Added

- Complete test coverage for both happy and sad cases.

### Removed

- Remove unused `Timex` dependency.

### Fixed

- Allow literal "|" in parameters by switching the delimiter to "unit separator".

## [0.1.4] - 2020-05

### Added

- Some test coverage for `Wiki.Action`.
- Add lint jobs to git hook and GitLab CI.

### Removed

- **Breaking:** Removed the incomplete `Wiki.Rest` client.

### Fixed

- Fix application configuration bug, nothing worked out of the box.
- Fix a continuation bug which would drop the first response.

## [0.1.2] - 2020-05

### Changed

- **Breaking:** Rename WikiAction -> `Wiki.Action`
- **Breaking:** Rename WikiRest -> `Wiki.Rest`
- **Breaking:** Rename WikiSSE -> `Wiki.EventStreams`

### Added

- Basic ORES client.
- Inline examples included in module documentation.
- Pipe-join list values for Action API.
- Accumulate Action results.

## [0.1.1] - 2020-05

### Changed

- Simplify and package examples as scripts.
- Host code on GitLab, apply the most basic CI.
- Temporarily inline the [cwc/eventsource\_ex](https://github.com/cwc/eventsource_ex/) server-side events library as a workaround.
- Switch to Tesla HTTP client.

### Added

- Send User-Agent header.
- Action API and continuations.  Authentication, session cookies.
- Pipelining.
- Flexible endpoint.
- Server-side events relayed as a `Stream`.
- Begin work on REST API.

## [0.1.0] - 2019-05

_Initial release._

[Unreleased]: https://gitlab.com/adamwight/mediawiki_client_ex/-/compare/v0.4.3...HEAD
[0.4.3]: https://gitlab.com/adamwight/mediawiki_client_ex/-/compare/v0.4.2...v0.4.3
[0.4.2]: https://gitlab.com/adamwight/mediawiki_client_ex/-/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/adamwight/mediawiki_client_ex/-/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/adamwight/mediawiki_client_ex/-/compare/v0.3.1...v0.4.0
[0.3.1]: https://gitlab.com/adamwight/mediawiki_client_ex/-/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/adamwight/mediawiki_client_ex/-/compare/v0.2.6...v0.3.0
[0.2.6]: https://gitlab.com/adamwight/mediawiki_client_ex/-/compare/v0.2.5...v0.2.6
[0.2.5]: https://gitlab.com/adamwight/mediawiki_client_ex/-/compare/v0.2.2...v0.2.5
[0.2.2]: https://gitlab.com/adamwight/mediawiki_client_ex/-/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/adamwight/mediawiki_client_ex/-/compare/v0.1.4...v0.2.1
[0.1.4]: https://gitlab.com/adamwight/mediawiki_client_ex/-/compare/v0.1.2...v0.1.4
[0.1.2]: https://gitlab.com/adamwight/mediawiki_client_ex/-/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/adamwight/mediawiki_client_ex/-/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/adamwight/mediawiki_client_ex/-/tags/v0.1.0
