defmodule ActionTest do
  use ExUnit.Case, async: true

  import Mox

  alias Tesla.Env
  alias Wiki.Action
  alias Wiki.Action.Session
  alias Wiki.Error
  alias Wiki.Tests.TeslaAdapterMock

  @url "https://dewiki.test/w/api.php"

  setup :verify_on_exit!

  defp create_session(opts \\ []) do
    site = opts[:site] || @url
    opts = Keyword.put(opts, :adapter, TeslaAdapterMock)
    Action.new(site, opts)
  end

  test "parses site structure" do
    site = %Wiki.SiteMatrix.Spec{base_url: "https://aawiki.test", dbname: "aawiki"}

    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      assert env.url == "https://aawiki.test/w/api.php"

      {:ok,
       %Env{
         env
         | body: %{bar: :baz},
           headers: [{"content-type", "application/json; charset=utf-8"}],
           status: 200
       }}
    end)

    create_session(site: site) |> Action.get!(foo: :bar)
  end

  test "returns new session" do
    %{__client__: client} = create_session()
    assert length(client.pre) >= 1
  end

  test "gets successfully" do
    canned_response = %{
      "batchcomplete" => "",
      "query" => %{
        "general" => %{
          "mainpage" => "Main Page"
        }
      }
    }

    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      {{_, user_agent}, _} = List.keytake(env.headers, "user-agent", 0)
      assert String.match?(user_agent, ~r/mediawiki_client_ex.*\d.*/)
      assert env.method == :get

      assert env.query == [
               action: :query,
               format: :json,
               formatversion: 2,
               meta: :siteinfo,
               siprop: :general
             ]

      headers = [{"content-type", "application/json; charset=utf-8"}]

      {:ok, %Env{env | body: canned_response, headers: headers, status: 200}}
    end)

    session =
      create_session(accumulate: true)
      |> Action.get!(
        action: :query,
        meta: :siteinfo,
        siprop: :general
      )

    assert session.result["query"]["general"]["mainpage"] == "Main Page"
  end

  test "merges previous results" do
    canned_response = %{
      "batchcomplete" => "",
      "query" => %{
        "general" => %{
          "mainpage" => "Main Page"
        },
        "statistics" => %{
          "activeusers" => 20_132
        }
      },
      "appended" => ["b"]
    }

    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      assert env.query == [
               action: :query,
               format: :json,
               formatversion: 2,
               meta: :siteinfo,
               siprop: "general|statistics"
             ]

      {:ok, %Env{env | body: canned_response, headers: [], status: 200}}
    end)

    session = create_session(accumulate: true)

    session = %Session{
      session
      | state: [
          accumulated_result: %{
            "appended" => ["a"],
            "isolated" => "foo",
            "query" => %{
              "general" => %{
                "mainpage" => "Main Page"
              },
              "statistics" => %{
                "merged" => true
              }
            }
          }
        ]
    }

    session =
      session
      |> Action.get!(
        action: :query,
        meta: :siteinfo,
        siprop: [:general, :statistics]
      )

    assert session.result["appended"] == ["a", "b"]
    assert session.result["isolated"] == "foo"
    assert session.result["query"]["statistics"]["activeusers"] == 20_132
    assert session.result["query"]["statistics"]["merged"] == true
  end

  test "doesn't collide literal pipes with separator" do
    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      assert env.query[:multivalue] == "\x1fFoo|Bar\x1fBaz"
      {:ok, %Env{env | status: 200, body: %{a: ~c"b"}}}
    end)

    create_session()
    |> Action.get!(multivalue: ["Foo|Bar", "Baz"])
  end

  test "posts using body, authenticates using cookie jar" do
    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      assert env.method == :get

      {:ok,
       %Env{
         env
         | body: %{
             "batchcomplete" => "",
             "query" => %{
               "tokens" => %{"logintoken" => "5c31497c51b4b28f2d6c19f3349070d25eccae52+\\"}
             }
           },
           headers: [
             {"set-cookie",
              "mediawiki_session=aam5aqq00euke0tn99fin92j4nnbueav; path=/; HttpOnly"}
           ],
           status: 200
       }}
    end)
    |> expect(:call, fn env, _opts ->
      assert env.method == :post
      assert env.query == []

      assert :zlib.gunzip(env.body) ==
               "action=login&format=json&formatversion=2&lgname=TestUser%40bot&lgpassword=botpass&lgtoken=5c31497c51b4b28f2d6c19f3349070d25eccae52%2B%5C"

      assert List.keyfind(env.headers, "cookie", 0) ==
               {"cookie", "a=b; mediawiki_session=aam5aqq00euke0tn99fin92j4nnbueav"}

      {:ok,
       %Env{
         env
         | body: %{
             "login" => %{"result" => "Success", "lguserid" => 1, "lgusername" => "TestUser"}
           },
           headers:
             List.keystore(
               env.headers,
               "set-cookie",
               0,
               {"set-cookie", "mediawiki_session=new_cookie; path=/; HttpOnly"}
             ),
           status: 200
       }}
    end)

    session =
      %Session{
        create_session()
        | state: [
            cookies: %{"a" => "b"}
          ]
      }
      |> Action.authenticate!(
        "TestUser@bot",
        "botpass"
      )

    assert session.state[:cookies] == %{"mediawiki_session" => "new_cookie", "a" => "b"}
  end

  test "posts using body, authenticates using cookie jar with empty default cookies" do
    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      assert env.method == :get

      {:ok,
       %Env{
         env
         | body: %{
             "batchcomplete" => "",
             "query" => %{
               "tokens" => %{"logintoken" => "5c31497c51b4b28f2d6c19f3349070d25eccae52+\\"}
             }
           },
           headers: [
             {"set-cookie",
              "mediawiki_session=aam5aqq00euke0tn99fin92j4nnbueav; path=/; HttpOnly"}
           ],
           status: 200
       }}
    end)
    |> expect(:call, fn env, _opts ->
      assert env.method == :post
      assert env.query == []

      assert :zlib.gunzip(env.body) ==
               "action=login&format=json&formatversion=2&lgname=TestUser%40bot&lgpassword=botpass&lgtoken=5c31497c51b4b28f2d6c19f3349070d25eccae52%2B%5C"

      assert List.keyfind(env.headers, "cookie", 0) ==
               {"cookie", "mediawiki_session=aam5aqq00euke0tn99fin92j4nnbueav"}

      {:ok,
       %Env{
         env
         | body: %{
             "login" => %{"result" => "Success", "lguserid" => 1, "lgusername" => "TestUser"}
           },
           headers:
             List.keystore(
               env.headers,
               "set-cookie",
               0,
               {"set-cookie", "mediawiki_session=new_cookie; path=/; HttpOnly"}
             ),
           status: 200
       }}
    end)

    session =
      create_session()
      |> Action.authenticate!(
        "TestUser@bot",
        "botpass"
      )

    assert session.state[:cookies] == %{"mediawiki_session" => "new_cookie"}
  end

  test "streams continuations" do
    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      {:ok,
       %Env{
         env
         | body: %{
             "batchcomplete" => "",
             "continue" => %{"continue" => "-||", "rccontinue" => "20200519061025|633"},
             "query" => %{
               "recentchanges" => [
                 %{"revid" => 616},
                 %{"revid" => 615}
               ]
             }
           },
           status: 200
       }}
    end)
    |> expect(:call, fn env, _opts ->
      assert env.query == [
               {:action, :query},
               {:format, :json},
               {:formatversion, 2},
               {:list, :recentchanges},
               {:rclimit, 2},
               {"continue", "-||"},
               {"rccontinue", "20200519061025|633"}
             ]

      {:ok,
       %Env{
         env
         | body: %{
             "batchcomplete" => "",
             "query" => %{
               "recentchanges" => [
                 %{"revid" => 614},
                 %{"revid" => 613}
               ]
             }
           },
           status: 200
       }}
    end)

    recent_changes =
      create_session()
      |> Action.stream(
        action: :query,
        list: :recentchanges,
        rclimit: 2
      )
      |> Enum.flat_map(fn response -> response["query"]["recentchanges"] end)
      |> Enum.map(fn rc -> rc["revid"] end)

    assert recent_changes == [616, 615, 614, 613]
  end

  test "handles 404 response" do
    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      {:ok,
       %Env{env | status: 404, body: "<html><head><title>404 Not Found</title></head></html>"}}
    end)

    assert_raise Error, ~r/404/, fn ->
      create_session()
      |> Action.get!(foo: :bar)
    end
  end

  test "handles network errors" do
    TeslaAdapterMock
    |> expect(:call, fn _env, _opts ->
      {:error, :econnrefused}
    end)

    assert_raise Error, ":econnrefused", fn ->
      create_session()
      |> Action.get!(foo: :bar)
    end
  end

  test "handles empty response" do
    ""
    |> failed_body_case("Empty response")
  end

  test "fails on malformed response" do
    "<html></html>"
    |> failed_body_case("Empty response")
  end

  test "handles API error in legacy format" do
    %{
      "error" => %{
        "info" => "Returned error"
      }
    }
    |> failed_body_case("Returned error")
  end

  test "handles API error with text" do
    %{
      "errors" => [
        %{"text" => "Returned error"}
      ]
    }
    |> failed_body_case("Returned error")
  end

  test "handles API error with html" do
    %{
      "errors" => [
        %{"html" => "<i>Returned error</i>"}
      ]
    }
    |> failed_body_case("<i>Returned error</i>")
  end

  test "handles API error with message spec" do
    %{
      "errors" => [
        %{"key" => "err-msg", "params" => [1, 2]}
      ]
    }
    |> failed_body_case("err-msg-1-2")
  end

  test "handles API error with code" do
    %{
      "errors" => [
        %{"code" => "err-code"}
      ]
    }
    |> failed_body_case("err-code")
  end

  defp failed_body_case(body, message) do
    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      {:ok, %Env{env | status: 200, body: body}}
    end)

    assert_raise Error, message, fn ->
      create_session() |> Action.get!(foo: :bar)
    end
  end

  test "can override user agent" do
    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      {{_, user_agent}, _} = List.keytake(env.headers, "user-agent", 0)
      assert user_agent == "foo bar"

      {:ok, %Env{env | body: %{bar: :baz}, status: 200}}
    end)

    create_session(user_agent: "foo bar")
    |> Action.get!(action: :query)
  end

  test "can disable compression" do
    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      refute Enum.any?(env.__client__.pre, fn {middleware, _, _} ->
               middleware === Tesla.Middleware.Compression
             end)

      {:ok, %Env{env | body: %{bar: :baz}, status: 200}}
    end)

    create_session(disable_compression: true)
    |> Action.get!(action: :query)
  end

  test "can enable compression with flag" do
    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      assert Enum.any?(env.__client__.pre, fn {middleware, _, _} ->
               middleware === Tesla.Middleware.Compression
             end)

      {:ok, %Env{env | body: %{bar: :baz}, status: 200}}
    end)

    create_session(disable_compression: false)
    |> Action.get!(action: :query)
  end

  test "can enable debug" do
    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      assert Enum.any?(env.__client__.pre, fn {middleware, _, _} ->
               middleware === Tesla.Middleware.Logger
             end)

      {:ok, %Env{env | body: %{bar: :baz}, status: 200}}
    end)

    create_session(debug: true)
    |> Action.get!(action: :query)
  end

  test "can enable accumulation" do
    TeslaAdapterMock
    |> expect(:call, fn env, _opts ->
      assert Enum.any?(env.__client__.pre, fn {middleware, _, _} ->
               middleware === Wiki.StatefulClient.CumulativeResult
             end)

      {:ok, %Env{env | body: %{bar: :baz}, status: 200}}
    end)

    create_session(accumulate: true)
    |> Action.get!(action: :query)
  end
end
