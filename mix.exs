defmodule Elixir.MixProject do
  use Mix.Project

  def project do
    [
      app: :mediawiki_client,
      version: "0.4.3",
      elixir: "~> 1.11",
      elixirc_paths: ~w(lib),
      start_permanent: Mix.env() == :prod,
      description: description(),
      deps: deps(),
      package: package(),
      source_url: "https://gitlab.com/adamwight/mediawiki_client_ex",
      dialyzer: [
        plt_add_apps: ~w(cookie gen_stage jason tesla)a,
        plt_core_path: "priv/plts/"
      ],
      docs: [
        extras: ~w(notebooks/demo.livemd CHANGELOG.md README.md),
        main: "readme"
      ],
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        "coveralls.html": :test,
        "test.all": :test
      ],
      aliases: aliases()
    ]
  end

  defp aliases do
    [
      "test.all": [
        "format --check-formatted",
        "clean",
        "compile --warnings-as-errors",
        "credo --strict",
        "coveralls.html",
        "dialyzer",
        "doctor"
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [extra_applications: [:logger]]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:cookie, "~> 0.0"},
      {:credo, "~> 1.0", only: :test, runtime: false},
      {:dialyxir, "~> 1.0", only: :test, runtime: false},
      {:doctor, "~> 0.0", only: :test, runtime: false},
      {:eventsource_ex, "~> 1.1"},
      {:ex_doc, "~> 0.0", only: :dev, runtime: false},
      {:excoveralls, "~> 0.14", only: :test, runtime: false},
      {:gen_stage, "~> 1.0"},
      {:httpoison, "~> 1.0"},
      {:jason, "~> 1.0"},
      {:mime, "~> 1.0"},
      {:mox, "~> 1.0", only: :test},
      {:tesla, "~> 1.0"}
    ]
  end

  defp description do
    "API client library for any MediaWiki-based site, such as Wikipedia or Wikimedia Commons. Provides the Action API, realtime feed ingestion, and ORES scoring."
  end

  defp package do
    [
      files: ~w(.formatter.exs lib mix.exs *.md),
      name: :mediawiki_client,
      maintainers: ["adamwight"],
      licenses: ["Apache-2.0"],
      links: %{
        "GitLab" => "https://gitlab.com/adamwight/mediawiki_client_ex",
        "Other MediaWiki Action API clients" => "https://www.mediawiki.org/wiki/API:Client_code"
      }
    ]
  end
end
